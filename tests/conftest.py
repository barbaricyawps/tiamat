# Import python libs
import mock
import os
import sys
import shutil
import pytest


@pytest.fixture(scope="function")
def hub(hub):
    hub.pop.sub.add(dyne_name="tiamat")
    yield hub


CWD = os.path.dirname(__file__)
PDIR = os.path.join(CWD, "pb")


@pytest.fixture
def pb_conf():
    yield os.path.join(PDIR, "pb.yml")


@pytest.fixture
def pb_bin():
    cwd = os.getcwd()
    build_dir = os.path.join(PDIR, "build")
    dist_dir = os.path.join(PDIR, "dist")
    os.chdir(PDIR)

    with mock.patch("sys.path", sys.path + [PDIR, os.path.abspath(f"{CWD}/..")]):
        yield os.path.join(dist_dir, "pb")
    os.chdir(cwd)
    shutil.rmtree(dist_dir, ignore_errors=True)
    shutil.rmtree(build_dir, ignore_errors=True)
